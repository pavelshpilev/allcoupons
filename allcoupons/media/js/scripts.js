function sticky_relocate() {
	var window_top = $(window).scrollTop();
	var div_top = $('#sticky-anchor').offset().top;
	if (window_top > div_top){
		$('.section').addClass('stick');
		$('.back-to-top').addClass('stick');
		$('.deal-panel').addClass('stick');
	}
	else {
		$('.section').removeClass('stick');
		$('.back-to-top').removeClass('stick');
		$('.deal-panel').removeClass('stick');
	}
}

google.setOnLoadCallback(
	function() {
		$(window).scroll(sticky_relocate);
		sticky_relocate();
	}
);
		
$(document).ready(function() {
	$('a#totop').smoothScroll();
	$().alert()
});	

$('#totop').smoothScroll();
from allcoupons.settings import DEAL_EXPIRES
from allcoupons.deals_parser.parser import AbstractParser
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from urllib2 import urlopen
import logging

logger = logging.getLogger()

class GrabOneParser(AbstractParser):

	SOURCE_NAME = 'GrabOne'
	URL = 'http://www.grabone.co.nz'
	FEED_URL = 'http://www.grabone.co.nz/home'

	soldout = False

	def getDealList(self):
		logger.info('Running %s parser through all deals.' % self.SOURCE_NAME)
		feed_page = urlopen(self.FEED_URL)
		feed_soup = BeautifulSoup(feed_page)
		feed_content = feed_soup.find('div', id="active-deals-holder")
		return feed_content.findAll('div', "deal")

	def getDealUrl(self, deal):
		return self.URL + deal.findNext('a', "row-link")['href']
	
	def getSoupWrapper(self, soup):
		return soup.find('div', id="dealShow").findNext(
			'div', "shadow-container").findNext(
				'div', "shadow-sides").findNext(
					'div', "shadow-box")

	def getTitle(self, soup):
		block = self.getSoupWrapper(soup).findNext('div', "todays-deal")
		return (block.h1.getText().strip() + ' ' +
			block.findNext('h3', "deal-title").getText().strip())

	def getDescription(self, soup):
		return self.getSoupWrapper(soup).findNext(
			'div', "top-right-column").findNext(
				'div', "full-description").prettify()

	def getHighlights(self, soup):
		return self.getSoupWrapper(soup).findNext(
			'div', "top-right-column").findNext(
				'div', "highlights").ul.prettify()

	def getConditions(self, soup):
		ret = soup.find('div', id="dealShow").findNext(
			'div', "shadow-container").findNext(
				'div', "shadow-sides").findNext(
					'div', "bottom-full-width").find(
						'div', id="deal-page-conditions").prettify()
		return ret[ret.find('</h3>')+6:ret.find('<span>')]

	def getImage(self, soup):
		return self.getSoupWrapper(soup).findNext(
			'div', "top-right-column").findNext(
				'div', "main-image").img['src']

	def getRegion(self, soup):
		return soup.find(
			'div', id="nav").find(
				'div', id="main-menu").findNext(
					'li', "cityName").find(
						'a', id="change").getText().strip().replace(' - ', '-')

	def getPrice(self, soup):
		return self.getSoupWrapper(soup).find(
			'div', id="buy-button-container").findNext(
				'span', "big-price").findNext(
					'span').getText().strip().replace(',', '')[1:]

	def getDiscount(self, soup):
		try:
			return self.getSoupWrapper(soup).findNext(
				'div', "top-left-column-wrap").findNext(
					'div', "top-left-column").findNext(
						'div', "discount-purchased").findNext(
							'span', "discount").findNext(
								'span', "big-price-span").getText().strip()[:-1]
		except:
			return 0

	def getSavings(self, soup):
		try:
			return self.getSoupWrapper(soup).findNext(
				'div', "top-left-column-wrap").findNext(
					'div', "top-left-column").findNext(
						'div', "discount-purchased").findNext(
							'span', "saving").findNext(
								'span', "big-price-span").getText().strip().replace(',', '')[1:]
		except:
			return 0

	def getIsSoldOut(self, soup):
		try:
			soldout = soup.find('div', id="content").find(
				'div', id="sold-out-point")
			if soldout is not None:
				return True
		except:
			return False

	def getNumberBought(self, soup):
		return self.getSoupWrapper(soup).findNext(
			'div', "top-left-column-wrap").findNext(
				'div', "top-left-column").findNext(
					'div', "purchased-wrapper").find(
						'span', id="number-purchased").getText().strip().replace(',', '')

	def getEnds(self, soup):
		try:
			countdown = self.getSoupWrapper(soup).findNext(
			'div', "top-left-column-wrap").findNext(
				'div', "top-left-column").findNext(
					'div', "side-wrap").findNext(
						'div', "timeRemaining").findNext(
							'div', "countdown")
			ret = int(countdown.findNext('span', "count-down-hours").getText().strip()) * 60 * 60
			ret += int(countdown.findNext('span', "count-down-mins").getText().strip()) * 60
			ret += int(countdown.findNext('span', "count-down-secs").getText().strip())
		except:
			# Set end time equal to expiration, hopping it will be updated with next parser run.
			ret = DEAL_EXPIRES * 60
		return datetime.now() + timedelta(seconds=ret)

	def getCategory(self, soup):
		ret = set()
		try:
			ret.add(soup.find('div', id="dealShow").findNext(
				'div', "shadow-container").findNext(
					'div', "shadow-top-tabbed").findNext(
						'ul').findNext(
							'li', "selected").a.getText().strip())
		except:
			scripts = soup.findAllPrevious('script', type="text/javascript")
			for script in scripts:
				if 'var nzhTrackerUrl =' in script.string:
					start = script.string.find('category=') + 9
					end = script.string.find('/', start)
					ret.add(script.string[start:end].replace('-', ' ').title())
					break
		return ret

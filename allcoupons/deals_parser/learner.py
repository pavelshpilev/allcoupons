from allcoupons.coupon.models import Coupon
import nltk
import re

class CategoryLearner():
	word_features = set()
	price_features = set()
	train_set = []

	# Initialise learner.
	# Run through all categorised deals - fetch deals' features, learn their categories.
	def __init__(self):
		coupons = Coupon.objects.filter(are_categories_guessed=False).filter(original_categories__assigned_category__isnull=False).order_by('id')
		for coupon in coupons:
			coupon_words = self._cleanText(nltk.clean_html(coupon.title).lower()).split()
			for word in coupon_words:
				if not (word.startswith('$') or word.endswith('%')):
					self.word_features.add(word)
			if coupon.price != None:
				self.price_features.add(self._getPriceGrade(coupon.price))
			if coupon.assigned_categories.all():
				for category in coupon.assigned_categories.all():
					for category_word in self._cleanText(category.name.lower()).split():
						self.word_features.add(category_word)
					self.train_set.append((self.couponFeatures(coupon), category.name))
			else:
				for category in coupon.original_categories.all():
					if category.assigned_category != None:
						for category_word in self._cleanText(category.assigned_category.name.lower()).split():
							self.word_features.add(category_word)
						self.train_set.append((self.couponFeatures(coupon), category.assigned_category.name))


	# Get price grade.
	# I.e. $13 = 10, $149 = 100, $270 = 200.
	def _getPriceGrade(self, price):
		try:
			price = int(price)
			#price = int(filter(lambda x: x.isdigit(), price))
			power = 0
			grade = pow(1*10, power)
			while price >= grade:
				if ((price/grade) >= 1 and (price/grade) < 10):
					return (price/grade)*grade
				power += 1
				grade = pow(1*10, power)
		except:
			return

	def _cleanText(self, text):
		return re.sub('[^A-Za-z0-9 $%]+', ' ', text)


	def classifier(self):
		if self.train_set:
			return nltk.NaiveBayesClassifier.train(self.train_set)
		return None


	# Extract deal's features: key-words and prices.
	def couponFeatures(self, coupon):
		features = {}
		coupon_words = set(self._cleanText(nltk.clean_html(coupon.title).lower()).split())
		if coupon.assigned_categories.all():
			for category in coupon.assigned_categories.all():
				for category_word in self._cleanText(category.name.lower()).split():
					coupon_words.add(category_word)
		else:
			for category in coupon.original_categories.all():
				if category.assigned_category != None:
					for category_word in self._cleanText(category.assigned_category.name.lower()).split():
						coupon_words.add(category_word)
		for word in coupon_words:
			if not (word.startswith('$') or word.endswith('%')):
				if word in self.word_features:		
					features['contains(%s)' % word] = True
		if coupon.price != None:
			price_grade = self._getPriceGrade(coupon.price)
		if price_grade in self.price_features:
			features['price_grade(%s)' % price_grade] = True
		return features
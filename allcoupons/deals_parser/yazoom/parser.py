from allcoupons.deals_parser.parser import AbstractParser
from allcoupons.settings import DEAL_EXPIRES
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from urllib2 import urlopen
import logging

logger = logging.getLogger()

class YazoomParser(AbstractParser):

	SOURCE_NAME = 'Yazoom'
	URL = 'http://www.yazoom.co.nz'
	FEED_URL = 'http://www.yazoom.co.nz'

	def getDealList(self):
		logger.info('Running %s parser through all deals.' % self.SOURCE_NAME)
		feed_regions_page = urlopen(self.FEED_URL)
		feed_regions_soup = BeautifulSoup(feed_regions_page)
		feed_regions = feed_regions_soup.find('html').find(
			'body').findNext(
				'div', "header").findNext(
					'div', "contain").findNext(
						'div', "dropdown").findNext(
							'div', "drop").findNext(
								'ul').findAll('a')
		ret = []
		for region in feed_regions:
			feed_page = urlopen(self.FEED_URL + region['href']+'/all')
			feed_soup = BeautifulSoup(feed_page)
			feed_content = feed_soup.find('div', id="maincontain").findNext(
				'div', "contain").findNext(
					'div', "fullbox").findNext(
						'div', "holder").findAll(
							'div', "all-deal-box")
			ret += feed_content
		return ret

	def getDealUrl(self, deal):
		return deal.findNext('div', "content").h3.a['href']
	
	def getSoupWrapper(self, soup):
		return soup.find('div', id="maincontain").findNext(
			'div', "maincol").findNext(
				'div', "content")

	def getTitle(self, soup):
		return self.getSoupWrapper(soup).h1.getText().strip()

	def getDescription(self, soup):
		return self.getSoupWrapper(soup).findNext(
			'div', "bigpanel").findNext(
				'div', "dealtext").prettify().replace('More Information...', '')

	def getHighlights(self, soup):
		ret = self.getSoupWrapper(soup).findNext(
			'div', "textpanel").findNext(
				'div', "dealtext")
		while ret.h3.getText().strip() != 'Highlights':
			ret = ret.findNext(
				'div', "textpanel").findNext(
					'div', "dealtext")
		# Sometimes it's not <ul>
		try:
			return ret.ul.prettify()
		except:
			try:
				return ret.p.prettify()
			except:
				try:
					return ret.div.prettify()
				except:
					return ret.span.prettify()

	def getConditions(self, soup):
		ret = self.getSoupWrapper(soup).findNext(
			'div', "textpanel").findNext(
				'div', "dealtext")
		while ret.h3.getText().strip() != 'Conditions':
			ret = ret.findNext(
				'div', "textpanel").findNext(
					'div', "dealtext")
		return ret.findNext('p').prettify()

	def getImage(self, soup):
		return self.getSoupWrapper(soup).find(
			'div', id="slider-container").find(
				'div', id="slider").findNext(
					'div', "main-image").img['src']

	def getRegion(self, soup):
		return soup.find('html').find(
			'body').findNext(
				'div', "header").findNext(
					'div', "contain").h1.getText().strip().replace(' - ', '-')

	def getPrice(self, soup):
		try:
			price =  self.getSoupWrapper(soup).findNext(
				'div', "smlpanel").findNext(
					'div', "buynow").findNext(
						'div' ,"fullprice").findNext(
							'span', "price").getText().replace(',', '')[1:]
		except:
			price = None
		finally:
			return price

	def getDiscount(self, soup):
		try:
			ret = self.getSoupWrapper(soup).findNext(
				'div', "smlpanel").findNext(
					'div', "buynow").findNext(
						'div' ,"valhold").findNext(
							'div' ,"val")
			while ret.span.getText() != 'Discount':
				ret = ret.findNext('div' ,"val")
			return ret.getText()[10:-1]
		except:
			return 0

	def getSavings(self, soup):
		try:
			ret = self.getSoupWrapper(soup).findNext(
				'div', "smlpanel").findNext(
					'div', "buynow").findNext(
						'div' ,"valhold").findNext(
							'div' ,"val")
			while ret.span.getText() != 'You Save':
				ret = ret.findNext('div' ,"val")
			return ret.getText().replace(',', '')[11:]
		except:
			return 0

	def getIsSoldOut(self, soup):
		try:
			return (
				'buysoldout' in
				self.getSoupWrapper(soup).findNext(
					'div', "smlpanel").findNext(
						'div', "buynow").a['class']
			)
		except:
			return False

	def getNumberBought(self, soup):
		try:
			return self.getSoupWrapper(soup).findNext(
				'div', "smlpanel").findNext(
					'div', "dealsold").findNext(
						'h2').getText().strip()[:-7].replace(',', '')
		except:
			return 0

	def getEnds(self, soup):
		try:
			scripts = soup.findAllPrevious('script', type="text/javascript")
			for script in scripts:
				if 'var countdownTimer = initializeTimer' in script.getText():
					start = script.getText().find('var countdownTimer = initializeTimer') + 37
					end = script.getText().find(',' ,start)
					time_left = int(script.getText()[start:end])
					break
		except:
			time_left = DEAL_EXPIRES * 60
		finally:
			return datetime.now() + timedelta(seconds=time_left)

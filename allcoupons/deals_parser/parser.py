from allcoupons.coupon.models import Coupon, OriginalCategory, OriginalRegion, \
	AssignedCategory
from allcoupons.settings import DEAL_TTL
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from django.core.files.base import File
from django.core.files.temp import NamedTemporaryFile
from urllib2 import urlopen
import logging
import xml.sax.saxutils as saxutils

logger = logging.getLogger()

class AbstractParser():

	SOURCE_NAME = None
	URL = None
	FEED_URL = None
	counter_new = 0
	counter_update = 0
	counter_fail = 0

	def getDealList(self):
		raise NotImplementedError

	def getDealUrl(self, deal):
		raise NotImplementedError

	def getSoup(self, deal):
		try:
			return BeautifulSoup(urlopen(self.getDealUrl(deal)))
		except:
			return None

	def getSoupWrapper(self, soup):
		raise NotImplementedError

	def _logResult(self):
		if self.counter_fail > 0:
			logger.error('Failed fetching %s deals from %s' % (self.counter_fail, self.SOURCE_NAME))
		if self.counter_new > 0 or self.counter_update > 0:
			logger.info('%s new deals fetched and %s updated from %s' % (self.counter_new, self.counter_update, self.SOURCE_NAME))
		else:
			logger.info('No deals updated from %s' % self.SOURCE_NAME)

	def runParser(self, learner=None):
		deal_list = self.getDealList()
		for deal in deal_list:
			try:
				coupon = Coupon.objects.get(url=self.getDealUrl(deal))
				exists = True
			except Coupon.DoesNotExist:
				coupon = None
				exists = False
			if (
				not exists
				or (
					exists
					and ((not coupon.is_sold_out and coupon.ends >= datetime.now()) or datetime.now() >= coupon.ends)
					and (timedelta(minutes=DEAL_TTL) <= datetime.now() - coupon.last_parsed)
				)
			):
				self._parseDeal(deal, coupon, learner)
				if exists:
					self.counter_update += 1
				else:
					self.counter_new += 1
		self._logResult()

	def _saveCoupon(self, soup, coupon, url, learner):
		if self.getIsSoldOut(soup):
			if coupon:
				coupon.expire()
				logger.info('%s marked as sold out.' % coupon.url)
		else:
			# Getting image. Has to be above updating coupon, as comparing last parsed time
			img_url = self.getImage(soup)
			image_updated = self._getImageIfUpdated(coupon, img_url)
			# Updating existent coupon
			if coupon and not coupon.is_sold_out:
				coupon.title = saxutils.unescape(self.getTitle(soup))
				coupon.description = saxutils.unescape(self.getDescription(soup))
				coupon.highlights = saxutils.unescape(self.getHighlights(soup))
				coupon.conditions = saxutils.unescape(self.getConditions(soup))
				coupon.price = self.getPrice(soup)
				coupon.is_price_from = self.getIsPriceFrom(soup)
				coupon.discount = self.getDiscount(soup)
				coupon.savings = self.getSavings(soup)
				coupon.number_bought = self.getNumberBought(soup)
				coupon.ends = self.getEnds(soup)
				coupon.last_parsed = datetime.now()
				coupon.save()
			# Fetching new/replacing expired coupon
			else:
				# Deleting old expired coupon if new has the same url
				if coupon and coupon.is_sold_out:
					coupon.delete()
				coupon = self._newCoupon(soup, url, learner)
			# Saving image
			if image_updated:
				# Deleting old image before saving a new one
				# Empty file field is always an empty string in DB
				# https://code.djangoproject.com/ticket/10244
				if coupon.original_image.name != '':
					coupon.deleteImage()
				img_temp = NamedTemporaryFile(delete=True)
				img_temp.write(image_updated.read())
				img_temp.flush()
				coupon.original_image.save(img_url.split('/')[-1].split('?')[0], File(img_temp), save=True)
				# Pregenerate thumbnails
				coupon.thumbnail.url
				coupon.image.url
		return coupon

	def _newCoupon(self, soup, url, learner):
		# Getting original categories and region.
		region, region_created = OriginalRegion.objects.get_or_create(name=saxutils.unescape(self.getRegion(soup)))
		if region_created:
			logger.warn('Adding new region: %s' % region.name)
		categories = set()
		for cat in self.getCategory(soup):
			category, category_created = OriginalCategory.objects.get_or_create(name=saxutils.unescape(cat))
			if category_created:
				logger.warn('Adding new category: %s' % category.name)
			category.save()
			categories.add(category)
		are_categories_guessed = (learner != None and not categories)
		# Saving coupon
		coupon = Coupon(
			url = url,
			source = self.SOURCE_NAME,
			title = saxutils.unescape(self.getTitle(soup)),
			description = saxutils.unescape(self.getDescription(soup)),
			highlights = saxutils.unescape(self.getHighlights(soup)),
			conditions = saxutils.unescape(self.getConditions(soup)),
			original_region = region,
			price = self.getPrice(soup),
			is_price_from = self.getIsPriceFrom(soup),
			discount = self.getDiscount(soup),
			savings = self.getSavings(soup),
			is_sold_out = False,
			number_bought = self.getNumberBought(soup),
			ends = self.getEnds(soup),
			first_parsed = datetime.now(),
			are_categories_guessed = are_categories_guessed
		)
		coupon.save()
		# Assigning categories to coupon
		if categories:
			for category in categories:
				coupon.original_categories.add(category)
			coupon.save()
		else:
			if are_categories_guessed and learner.classifier():
				category_guess = learner.classifier().classify(learner.couponFeatures(coupon))
				coupon.assigned_categories.add(AssignedCategory.objects.get(name=category_guess))
				logger.info('Guessing category: %s' % category_guess)
			coupon.save()
		return coupon

	def _getImageIfUpdated(self, coupon, img_url):
		remote_img = urlopen(img_url)
		# Always return image for new coupon
		if coupon is None:
			return remote_img
		# Trying to get image last modified date. This not always present in HTTP response.
		try:
			img_modified = datetime.strptime(remote_img.info().get('Last-Modified'), '%a, %d %b %Y %H:%M:%S %Z')
			if img_modified > coupon.last_parsed:
				return remote_img
		except:
			pass
		# Trying to get image size.
		try:
			img_size = int(remote_img.info().get('Content-Length'))
			if coupon.original_image.size == img_size:
				return None
		except:
			pass
		return remote_img

	def _parseDeal(self, deal, coupon, learner):
		url = self.getDealUrl(deal)
		logger.debug('Fetching %s' % url)
		soup = self.getSoup(deal)
		if soup:
			try:
				return self._saveCoupon(soup, coupon, url, learner)
			except:
				self.counter_fail += 1
				logger.exception('Filed to parse %s' % url)
		else:
			logger.warn('Failed to fetch %s. Skipping.' % url)

	def getId(self, coupon):
		if coupon:
			return coupon.id

	def getFirstParsed(self, coupon):
		if coupon:
			return coupon.first_parsed
		return datetime.now()

	def getTitle(self, soup):
		raise NotImplementedError

	def getDescription(self, soup):
		raise NotImplementedError

	def getHighlights(self, soup):
		raise NotImplementedError

	def getConditions(self, soup):
		raise NotImplementedError

	def getImage(self, soup):
		raise NotImplementedError

	def getRegion(self, soup):
		raise NotImplementedError

	def getPrice(self, soup):
		raise NotImplementedError

	def getIsPriceFrom(self, soup):
		return False

	def getDiscount(self, soup):
		raise NotImplementedError

	def getSavings(self, soup):
		raise NotImplementedError

	def getIsSoldOut(self, soup):
		return False

	def getNumberBought(self, soup):
		raise NotImplementedError

	def getEnds(self, soup):
		raise NotImplementedError

	def getCategory(self, soup):
		return set()
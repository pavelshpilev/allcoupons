from allcoupons.deals_parser.parser import AbstractParser
from datetime import datetime
import pytz

import logging
logger = logging.getLogger()

# Parsing libraries
from urllib2 import urlopen
from xml.dom import minidom
from bs4 import BeautifulSoup

class GrouponNzParser(AbstractParser):

	SOURCE_NAME = 'Groupon NZ'
	URL = 'http://www.grouponnz.co.nz'
	FEED_URL = 'http://api-asia.groupon.de/api/v1/deals/oftheday/NZ'

	def getDealList(self):
		logger.info('Running %s parser through all deals.' % self.SOURCE_NAME)
		feed_page = urlopen(self.FEED_URL)
		feed_soup = minidom.parse(feed_page)
		all_deals = feed_soup.getElementsByTagName('deals')
		all_deals = all_deals[0].childNodes
		# Skipping Auckland Special deals that are duplicated under Auckland.
		ret = set()
		ret_titles = set()
		auckland_special = set()
		for deal in all_deals:
			for nodes in deal.childNodes:
				if nodes.nodeName == 'city':
					if nodes.getAttribute('name') != 'Auckland Special':
						ret.add(deal)
						ret_titles.add(deal.getAttribute('title'))
					else:
						auckland_special.add(deal)
					break
		for as_deal in auckland_special:
			if as_deal.getAttribute('title') not in ret_titles:
				ret.add(as_deal)
		return ret

	def getDealUrl(self, deal):
		return deal.getAttribute('url')

	def getSoup(self, deal):
		return deal

	def getTitle(self, soup):
		return soup.getAttribute('title')

	def getDescription(self, soup):
		page = urlopen(soup.getAttribute('url'))
		html_soup = BeautifulSoup(page)
		return html_soup.find(
			'article', id="content").findNext(
				'section', "contentBoxNormal").findNext(
					'div', "contentDealData").findNext(
						'div', "contentBoxNormalLeft").findNext(
							'div', "contentDealDetails").prettify()

	def getHighlights(self, soup):
		return soup.getAttribute('highlights')

	def getConditions(self, soup):
		return soup.getAttribute('conditions')

	def getImage(self, soup):
		return soup.getAttribute('image_large_url')

	def getRegion(self, soup):
		for child in soup.childNodes:
			if child.nodeName == 'city':
				return child.getAttribute('name').replace(' - ', '-')

	def getPrice(self, soup):
		return soup.getAttribute('price')

	def getIsPriceFrom(self, soup):
		for child in soup.childNodes:
			if child.nodeName == 'options':
				return True
		return False

	def getDiscount(self, soup):
		return soup.getAttribute('discount_percent')

	def getSavings(self, soup):
		return soup.getAttribute('discount_amount')

	def getIsSoldOut(self, soup):
		if soup.getAttribute('sold_out') == 'true':
			return True
		return False

	def getNumberBought(self, soup):
		ret = int(soup.getAttribute('sold_count'))
#		for child in soup.childNodes:
#			if child.nodeName == 'options':
#				for nested in child.childNodes:
#					if nested.nodeName == 'deal':
#						ret += int(nested.getAttribute('sold_count'))
		return ret

	def getEnds(self, soup):
		return datetime.strptime(soup.getAttribute('end'), "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc).astimezone(pytz.timezone('Australia/Sydney')).strftime("%Y-%m-%d %H:%M:%S")

	def getCategory(self, soup):
		ret = set()
		for child in soup.childNodes:
			if child.nodeName == 'tags':
				tag = child.firstChild
				while tag.nodeName == 'tag':
					if tag.getAttribute('type').lower() == 'category':
						ret.add(tag.getAttribute('translation'))
					tag = tag.nextSibling
					if tag == None:
						break
		return ret

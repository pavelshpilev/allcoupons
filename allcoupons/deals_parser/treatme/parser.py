from allcoupons.deals_parser.parser import AbstractParser
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from urllib2 import urlopen
import logging

logger = logging.getLogger()

class TreatMeParser(AbstractParser):

	SOURCE_NAME = 'TreatMe'
	URL = 'http://treatme.co.nz'
	FEED_URL = 'http://treatme.co.nz/TodaysDeals'

	def getDealList(self):
		logger.info('Running %s parser through all deals.' % self.SOURCE_NAME)
		feed_page = urlopen(self.FEED_URL)
		feed_soup = BeautifulSoup(feed_page)
		feed_content = feed_soup.find('div', id="todaysTreats")
		return feed_content.findAll('div', "treat")

	def getDealUrl(self, deal):
		return self.URL + deal.findNext('a', "cont")['href']

	def getSoupWrapper(self, soup):
		return soup.find('div', id="colMain").findNext(
			'div', "main-deal")

	def getTitle(self, soup):
		return self.getSoupWrapper(soup).h1.getText().strip()

	def getDescription(self, soup):
		return soup.find('div', id="voucherDescription").prettify()

	def getHighlights(self, soup):
		ret = self.getSoupWrapper(soup).findNext(
			'div', "colRight").findNext(
				'div', "colHalf")
		while 'Highlights' not in ret.findNext('h2', "signPainter").getText():
			ret = ret.findNext('div', "colHalf")
		return ret.findNext('div', "dealinfo").prettify()

	def getConditions(self, soup):
		ret = self.getSoupWrapper(soup).findNext(
			'div', "colRight").findNext(
				'div', "colHalf")
		while 'Conditions' not in ret.findNext('h2', "signPainter").getText():
			ret = ret.findNext('div', "colHalf")
		return ret.findNext('div', "dealinfo").prettify()

	def getImage(self, soup):
		try:
			return self.getSoupWrapper(soup).findNext(
				'div', "colRight").find(
					'img', id="mainDealImageForeground")['src']
		except:
			return self.getSoupWrapper(soup).findNext(
				'div', "colRight").findNext('img')['src']

	def getRegion(self, soup):
		return soup.find('div', id="region").h2.getText().strip().replace(' - ', '-')

	def getPrice(self, soup):
		return self.getSoupWrapper(soup).findNext(
			'div', "colLeft").find(
				'div', id="buy").findNext(
					'div', "title").h2.getText().strip().replace(',', '')[1:]

	def getDiscount(self, soup):
		try:
			ret = self.getSoupWrapper(soup).findNext(
				'div', "colLeft").find(
					'div', id="buy").findNext('dl')
			while ret.findNext('dt').getText().strip() != 'Discount':
				ret = ret.findNext('dl')
			return ret.findNext('dd').getText().strip()[:-1]
		except:
			return 0

	def getSavings(self, soup):
		try:
			ret = self.getSoupWrapper(soup).findNext(
				'div', "colLeft").find(
					'div', id="buy").findNext('dl')
			while ret.findNext('dt').getText().strip() != 'You Save':
				ret = ret.findNext('dl')
			return ret.findNext('dd').getText().strip().replace(',', '')[1:]
		except:
			return 0

	def getIsSoldOut(self, soup):
		try:
			return ('NO LONGER AVAILABLE' in self.getSoupWrapper(soup).find(
				'div', id="buy").find(
					'div', id="buyButton").a.getText().upper())
		except:
			return False

	def getNumberBought(self, soup):
		try:
			return self.getSoupWrapper(soup).find(
				'div', id="dealOn").findNext(
					'h4').findNext(
						'span', "green").getText().strip().replace(',', '')
		except:
			return 0

	def getEnds(self, soup):
		try:
			scripts = soup.findAllPrevious('script', type="text/javascript")
			for script in scripts:
				if '$(\'#time\').countdown({' in script.getText():
					start = script.getText().find('until: \'') + 8
					end = script.getText().find('\'' ,start)
					ret = int(script.getText()[start:end])
					break
		except:
			# 20 minutes - until next refresh
			ret = 20 * 60
		return datetime.now() + timedelta(seconds=ret)
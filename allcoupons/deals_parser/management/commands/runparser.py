from allcoupons.coupon.models import Coupon
from allcoupons.deals_parser.grabone.parser import GrabOneParser
from allcoupons.deals_parser.groupon.parser import GrouponNzParser
from allcoupons.deals_parser.treatme.parser import TreatMeParser
from allcoupons.deals_parser.yazoom.parser import YazoomParser
from allcoupons.deals_parser.learner import CategoryLearner
from allcoupons.settings import DEAL_EXPIRES
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
import logging

logger = logging.getLogger()

class Command(BaseCommand):
	args = '<parser parser ...>'
	help = 'Run AllCoupons Parser. Specify parser names as arguments to run only them. Will run through all parsers otherwise.'

	def handle(self, *args, **options):
		learner_model = None
		if 'yazoom' in args or 'treatme' in args or not args:
			learner_model = CategoryLearner()
		if 'groupon' in args or not args:
			GrouponNzParser().runParser()
		if 'grabone' in args or not args:
			GrabOneParser().runParser()
		if 'yazoom' in args or not args:
			YazoomParser().runParser(learner=learner_model)
		if 'treatme' in args or not args:
			TreatMeParser().runParser(learner=learner_model)
		# Expire deals that have ended but wasn't marked sold out
		for coupon in Coupon.objects.filter(is_sold_out=False):
			if datetime.now() - coupon.last_parsed >= timedelta(minutes=DEAL_EXPIRES):
				coupon.expire()
				logger.info('%s marked as sold out.' % coupon.url)
		self.stdout.write('Parsing complete\n')
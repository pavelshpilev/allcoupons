from datetime import datetime
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from imagekit.models import ImageSpecField
from imagekit.processors.resize import ResizeToFill
import os


# Generate dynamic file path in format 'coupons_imgs/2011-10/17/Groupon NZ/image.jpg'
def upload_path_handler(instance, filename):
	return os.path.join(
		'coupon_imgs',
		'%s' % instance.source.lower().replace(' ', '_'),
		'%s_%s' % (instance.pk, filename)
	)


class Coupon(models.Model):
	url = models.TextField('URL', unique=True)
	source = models.TextField()
	title = models.TextField()
	description = models.TextField(blank=True, null=True)
	highlights = models.TextField(blank=True, null=True)
	conditions = models.TextField(blank=True, null=True)
	original_image = models.ImageField(upload_to=upload_path_handler, blank=True) # File fields currently can't be set to null in DB. See https://code.djangoproject.com/ticket/10244
	thumbnail = ImageSpecField([ResizeToFill(180, 108)], image_field='original_image', options={'quality': 100, 'optimize':True})
	image = ImageSpecField([ResizeToFill(300, 180)], image_field='original_image', options={'quality': 100, 'optimize':True, 'progressive':True})
	original_region = models.ForeignKey('OriginalRegion', blank=True, null=True)
	price = models.DecimalField(max_digits=7, decimal_places=2)
	is_price_from = models.BooleanField(default=False)
	is_sold_out = models.BooleanField(default=False)
	number_bought = models.IntegerField(blank=True)
	discount = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
	savings = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
	first_parsed = models.DateTimeField()
	ends = models.DateTimeField()
	last_parsed = models.DateTimeField(default=datetime.now())
	original_categories = models.ManyToManyField('OriginalCategory', blank=True)
	assigned_categories = models.ManyToManyField('AssignedCategory', blank=True)
	are_categories_guessed = models.BooleanField(default=False)

	# Returns AssignedRegion.
	def getRegion(self):
		if self.original_region.assigned_region:
			return self.original_region.assigned_region
		return None

	# Returns categories. Original or assigned.
	def getCategories(self):
		if self.are_categories_guessed:
			return self.assigned_categories.all()
		else:
			categories = set()
			for category in self.original_categories.all():
				if category.assigned_category is not None:
					categories.add(category.assigned_category)
			return categories

	def expire(self):
		self.is_sold_out = True
		self.deleteImage()
		return self.save()

	def deleteImage(self):
	# As safe as possible - failure is possible on every step due to file missing or being already deleted.
		try:
			if self.thumbnail.name != '':
				self.thumbnail.storage.delete(self.thumbnail.path)
		except:
			pass
		try:
			if self.image.name != '':
				self.image.storage.delete(self.image.path)
		except:
			pass
		try:
			if self.original_image.namev != '':
				self.original_image.storage.delete(self.original_image.path)
		except:
			pass
		try:
			self.original_image.delete(save=False)
		except:
			pass

	def __unicode__(self):
		return self.title


# Removing image file with coupon
@receiver(pre_delete, sender=Coupon, weak=False)
def delete_image_on_coupon_deletion(sender, instance, **kwargs):
	instance.deleteImage()


class OriginalCategory(models.Model):
	name = models.CharField(max_length=500, unique=True)
	#source = models.ManyToManyField('SourceSite')
	assigned_category = models.ForeignKey('AssignedCategory', blank=True, null=True)

	def __unicode__(self):
		return self.name


class AssignedCategory(models.Model):
	name = models.CharField(max_length=500, unique=True)
	url = models.CharField(max_length=255, null=True, blank=True, unique=True)
	order = models.IntegerField(null=True, blank=True)

	def __unicode__(self):
		return self.name


class OriginalRegion(models.Model):
	name = models.CharField(max_length=255, unique=True)
	#source = models.ManyToManyField('SourceSite')
	assigned_region = models.ForeignKey('AssignedRegion', blank=True, null=True)

	def __unicode__(self):
		return self.name


class AssignedRegion(models.Model):
	name = models.CharField(max_length=255, unique=True)
	url = models.CharField(max_length=255, null=True, blank=True)
	territory = models.ForeignKey('Territory')
	hidden = models.BooleanField(default=False)
	order = models.IntegerField(null=True, blank=True)

	def __unicode__(self):
		return self.name


class Territory(models.Model):
	"""
	Used for storing island name or other geographic instance larger than region.
	"""
	name = models.CharField(max_length=255, unique=True)

	def __unicode__(self):
		return self.name

"""
class SourceSite(models.Model):
	name = models.CharField(max_length=255)
#	logo = models.FileField(upload_to='logos/', blank=True)
#	url = models.CharField('URL', max_length=255)
#	feed_url = models.CharField('Feed URL', max_length=255, blank=True)
#	regions = models.ManyToManyField('OriginalRegion', blank=True)
#	disabled = models.BooleanField(default=False)
#	history = models.BooleanField(default=False)

	def __unicode__(self):
		return self.name
"""
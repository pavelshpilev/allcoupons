from allcoupons.coupon.models import Coupon, AssignedRegion, AssignedCategory
from datetime import datetime
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic.detail import DetailView


def _getRegions(request, region=None):
	# Getting region list and current region
	current_region = request.session.get('region')
	regions = AssignedRegion.objects.filter(hidden=False).order_by('order')
	if region is not None:
		try:
			current_region = regions.get(url=region)
		except:
			pass
		if current_region != request.session.get('region') and current_region != None:
			request.session['region'] = current_region
	if current_region is None:
		current_region = regions.get(name='Auckland')
	return regions, current_region


def index(request, region=None, category=None):
	regions, current_region = _getRegions(request, region=region)
	# Getting current category
	try:
		current_category = AssignedCategory.objects.get(url=category)
	except:
		current_category = None
	# Sorting coupons in order requested
	sort = request.GET.get('sort')
	if sort not in ('price', '-savings', '-discount', '-number_bought'):
		sort = '-number_bought'
	coupons_all = Coupon.objects.filter(is_sold_out=False).filter(ends__gte=datetime.now()).order_by(sort)
	# Filtering coupons by region and category
	region_coupons = []
	for coupon in coupons_all:
		if coupon.getRegion() == current_region:
			region_coupons.append(coupon)
	# Building category list for region
	categories = set()
	for coupon in region_coupons:
		for category in coupon.getCategories():
			categories.add(category)
	# Rendering
	return render_to_response('index.html',
		{'regions': regions, 'current_region': current_region, 'coupons': region_coupons, 'categories': categories, 'current_category': current_category, 'sort': sort},
		context_instance=RequestContext(request)
	)


class CouponView(DetailView):
	context_object_name = "coupon"
	model = Coupon
	template_name = "coupon.html"

	def get_context_data(self, **kwargs):
		regions, current_region = _getRegions(self.request, region=None)
		context = super(CouponView, self).get_context_data(**kwargs)
		context['regions'] = regions
		context['current_region'] = current_region
		return context
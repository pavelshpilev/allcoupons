from allcoupons.coupon.models import Coupon, OriginalCategory, AssignedCategory, \
	OriginalRegion, AssignedRegion, Territory
from django.contrib import admin
from imagekit.admin import AdminThumbnail


class ThumbnailAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'admin_thumbnail')
	admin_thumbnail = AdminThumbnail(image_field='thumbnail')


admin.site.register(Coupon, ThumbnailAdmin)
admin.site.register(OriginalCategory)
admin.site.register(AssignedCategory)
admin.site.register(OriginalRegion)
admin.site.register(AssignedRegion)
admin.site.register(Territory)
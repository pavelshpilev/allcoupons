from django.template import Library

register = Library()

@register.filter(name='count')
def count(coupons, category=None):
	if category is None:
		return len(coupons)
	else:
		ret = 0
		for coupon in coupons:
			if category in coupon.getCategories():
				ret += 1
		return ret
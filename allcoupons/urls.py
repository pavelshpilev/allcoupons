import os
from allcoupons.coupon.models import Coupon
from allcoupons.coupon.views import CouponView
from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap
from django.views.generic.base import RedirectView
admin.autodiscover()

# Sitemap dictionaries
info_dict = {
	'queryset': Coupon.objects.all(),
	'date_field': 'last_parsed',
}
sitemaps = {
	'flatpages': FlatPageSitemap,
	'deals': GenericSitemap(info_dict, priority=0.4, changefreq='hourly'),
}

urlpatterns = patterns('',
	url(r'^$', 'allcoupons.coupon.views.index'),
	url(r'^deals/(?P<region>\w+)/$', 'allcoupons.coupon.views.index'),
	url(r'^deals/(?P<region>\w+)/(?P<category>\w+)/$', 'allcoupons.coupon.views.index'),
	url(r'^deal/(?P<pk>\d+)/$', CouponView.as_view(), name='deal_view'),

	url(r'^external/', include('external_links.urls')),

	url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
	url(r'^robots\.txt$', include('robots.urls')),
	url(r'^favicon\.ico$', RedirectView.as_view(url=os.path.join(settings.MEDIA_URL,'images/favicon.ico'))),

	url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	)
